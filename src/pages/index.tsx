import React from 'react';
import { GetServerSideProps } from 'next'
import { HomeProps } from './interfaces'
import styles from './styles.module.css'

// https://www.figma.com/file/nKGlBYbZnpcI3fsTufmxee/Move.it-2.0-(Copy)?node-id=160%3A2761

import Head from 'next/head'

import ExperienceBar from '../components/ExperienceBar'
import Profile from '../components/Profile'
import CompletedChallenges from '../components/CompletedChallenges'
import Countdown from '../components/Countdown'
import ChallengeBox from '../components/ChallengeBox';
import ChallengesProvider from '../contexts/ChallengesContext';

/*

1- Chuva de confete quando passa de level
2- Tela de cadastro de usuário (nome e foto[opção de url ou foto padrão, ou BONUS subir arquivo de foto])
3- Construir backend com o Next

*/

const Home = (props: HomeProps) => {
  return (
    <ChallengesProvider
      level={props.level} 
      currentExperience={props.currentExperience} 
      challengesCompleted={props.challengesCompleted}
    >
      <div className={styles.container}>
        <Head>
          <title>Inicio | move.it</title>
        </Head>
        
        <ExperienceBar />

        <section>
          <div>
            <Profile />
            <CompletedChallenges />
            <Countdown />
          </div>

          <div>
            <ChallengeBox />
          </div>
        </section>
      </div>
    </ChallengesProvider>
  );
}

export const getServerSideProps: GetServerSideProps = async(ctx) => {
  const { level, currentExperience, challengesCompleted } = ctx.req.cookies

  return {
    props: {
      level: Number(level), 
      currentExperience: Number(currentExperience), 
      challengesCompleted: Number(challengesCompleted),
    }
  }
}

export default Home;
