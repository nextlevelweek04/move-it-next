import '../styles/colors.css'
import '../styles/fontsize.css'
import '../styles/global.css'

const MyApp = ({ Component, pageProps }) => <Component {...pageProps} />
export default MyApp
