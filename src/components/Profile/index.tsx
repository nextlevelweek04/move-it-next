import { useContext } from 'react'
import { ChallengesContext } from '../../contexts/ChallengesContext'
import styles from './styles.module.css'
import { GiPlainArrow } from 'react-icons/gi'

const Profile = () => {
  const { level } = useContext(ChallengesContext)

  return (
    <div className={styles.profileContainer}>
      <img src="https://media-exp1.licdn.com/dms/image/C4D03AQEQTkST_F7UmA/profile-displayphoto-shrink_200_200/0/1615460085142?e=1623888000&v=beta&t=Etsc6WvY8lavMJQczIATv4W7xKEVp5lAwATRlykH4pM" alt="Imagem do Avatar"/>
      <div>
        <strong>Giovanni Pregnolato Rosim</strong>
        <p>
          <GiPlainArrow color={"var(--green)"} title="Level Up" />
          Level {level}
        </p>
      </div>
    </div>
  )
}

export default Profile