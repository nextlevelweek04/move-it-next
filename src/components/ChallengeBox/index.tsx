import { useContext } from 'react'
import styles from './styles.module.css'

import { ChallengesContext } from '../../contexts/ChallengesContext'

import { FaAngleDoubleUp, FaLightbulb, FaTrophy, FaGrinBeamSweat, FaGrinWink,
  FaRegEye as eye,
  FaPray as pray,
} from 'react-icons/fa'

import { 
  GiTalk as talk, 
  GiMeditation as meditation,
} from 'react-icons/gi'

import {
  IoHandLeftOutline as hand,
} from 'react-icons/io5'

import {
  BiBody as body
} from 'react-icons/bi'
import { ChallengeTypes } from '../../contexts/ChallengesContext/interface'
import { IconType } from 'react-icons'

const iconsComponents = {
  eye,
  body,
  hand,
  talk,
  meditation,
  pray,
} as { [K in ChallengeTypes]: IconType }

const ChallengeBox = () => {
  const { activeChallenge, resetChallenge, challengeCompleted } = useContext(ChallengesContext)

  const Icon = activeChallenge && iconsComponents[activeChallenge.type]

  return (
    <div className={styles.challengeBoxContainer}>
      {
        activeChallenge ? (
          <div className={styles.challengeActive}>
            <header>Ganhe {activeChallenge.amount} xp</header>

            <main>
              <FaTrophy size="6rem" color="var(--green)" />
              <strong>Novo desafio!</strong>
              <Icon size="4rem" color="var(--title)" title={activeChallenge.type} />
              <p>{activeChallenge.description}</p>
            </main>

            <footer>
              <button
                type="button"
                className={styles.challengeFailedButton}
                onClick={resetChallenge}
              >
                Falhei <FaGrinBeamSweat />
              </button>

              <button
                type="button"
                className={styles.challengeSucceededButton}
                onClick={() => challengeCompleted(activeChallenge.amount)}
              >
                Completei <FaGrinWink />
              </button>
            </footer>

          </div>
        ) : (
          <div className={styles.challengeNotActive}>
            <strong>Finalize o clico para receber um desafio!</strong>
            <p>
              <FaAngleDoubleUp size="6rem" color="var(--green)"/>
              Avance de level completando desafios...
            </p>
          </div>
        )
      }
    </div>
  )
}

export default ChallengeBox