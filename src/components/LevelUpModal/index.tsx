import { useContext } from 'react'
import { ChallengesContext } from '../../contexts/ChallengesContext'
import styles from './styles.module.css'
import { CgCloseO } from 'react-icons/cg'
import { GiLaurelCrown } from 'react-icons/gi'

const LevelUpModal = () => {
  const { closeLevelUpModal, level } = useContext(ChallengesContext)
  return (
    <div className={styles.overlay}>
      <div className={styles.container}>
        <header>
          <GiLaurelCrown />
          <label>{ level }</label>
        </header>

        <strong>Mandou bem!</strong>
        <p>Você alcançou um novo nível!!!</p>

        <button type="button" onClick={closeLevelUpModal}>
          <CgCloseO size={'2rem'} color="var(--title)" />
        </button>
      </div>
    </div>
  )
}

export default LevelUpModal