import { useEffect, useState, useContext } from 'react'
import styles from './styles.module.css'

import { ChallengesContext } from '../../contexts/ChallengesContext'

import { FaPlay, FaPause, FaRedoAlt, FaLaughBeam } from 'react-icons/fa'

const Countdown = ({ initialTime = 25*60 }) => {
  const { startNewChallenge, activeChallenge  } = useContext(ChallengesContext)

  const [time, setTime] = useState(initialTime)
  const [isActive, setIsActive] = useState(false)
  const [timeoutId, setTimeoutId] = useState(null)

  const minutes = Math.floor(time / 60)
  const [minuteLeft, minuteRight] = String(minutes).padStart(2, '0').split('')

  const seconds = time % 60
  const [secondLeft, secondRight] = String(seconds).padStart(2, '0').split('')

  useEffect(() => {
    if(isActive) setCountdown()
    else clearTimeout(timeoutId)
  }, [time, isActive])

  useEffect(() => {
    if(!activeChallenge) resetContdown()
  }, [activeChallenge])

  const setCountdown = () => setTimeoutId(
    setTimeout(() => {
      const newTime = time - 1 
      if(newTime > 0) {
        setTime((prev) => prev - 1)
        return
      }
      setTime(0)
      setIsActive(false)
      startNewChallenge()
    }, 1000)
  )

  const resetContdown = () => {
    setTime(initialTime)
    setIsActive(false)
  }

  return (
    <div>
      <div className={styles.countdownContainer}>
        <div>
          <span>{minuteLeft}</span>
          <span>{minuteRight}</span>
        </div>
        <span>:</span>
        <div>
          <span>{secondLeft}</span>
          <span>{secondRight}</span>
        </div>
      </div>

      {
        time <= 0 ?
        <button disabled className={styles.countdownButton}>
          Ciclo encerrado <FaLaughBeam color="var(--white)"/>
        </button>
        :
        (
          <>
            <button type="button" className={styles.countdownButton} onClick={() => setIsActive(!isActive)}>
              {
                isActive ? 'Pausar' 
                : 
                initialTime !== time ? 'Continuar' : 'Começar'
              } um ciclo {
                isActive ? <FaPause /> : <FaPlay />
              }
            </button>
            {
              isActive &&
              <button type="button" 
                className={`${styles.countdownButton} ${styles.countdownButtonActive}`} 
                onClick={resetContdown}
              >
                Recomeçar ciclo <FaRedoAlt />
              </button>
            }
          </>
        )
      }

    </div>
  )
}

export default Countdown