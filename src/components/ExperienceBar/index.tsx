import { useContext } from 'react'
import { ChallengesContext } from '../../contexts/ChallengesContext'

import styles from  './styles.module.css'

const ExperienceBar = () => {
  const { currentExperience, experienceCurrentLevel, experienceNextLevel } = useContext(ChallengesContext)

  const levelPercentValue = (100 *
    (currentExperience - experienceCurrentLevel) 
    / (experienceNextLevel - experienceCurrentLevel))
    
  const levelPercent = (levelPercentValue > 100 ? 100 : levelPercentValue).toString() + "%"

  return (
    <header className={styles.experienceBar}>
      <span>{experienceCurrentLevel}xp</span>
      <div>
        <div className={styles.currentExperience} style={{ width: levelPercent }} />
        <span className={styles.currentExperiencePoint} style={{ top: '50%', left: levelPercent }} />
        <span className={styles.currentExperienceValue} style={{ left: levelPercent }}>{currentExperience}px</span>
      </div>
      <span>{experienceNextLevel}px</span>
    </header>
  )
}

export default ExperienceBar