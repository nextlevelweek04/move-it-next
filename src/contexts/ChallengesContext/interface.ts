import { ReactNode } from "react";
import { HomeProps } from "../../pages/interfaces";

export interface ChallengeProviderProps extends HomeProps {
  children: ReactNode;
}

export interface ChallengeContextData {
  level: number;
  currentExperience: number;
  experienceCurrentLevel: number;
  experienceNextLevel: number;
  challengesCompleted: number;
  activeChallenge: Challenge; 
  levelUp: () => void;
  startNewChallenge: () => void;
  challengeCompleted: (exp: number) => void;
  resetChallenge: () => void;
  closeLevelUpModal: () => void
}

export interface Challenge {
  type: ChallengeTypes;
  description: string;
  amount: number;
}

export type ChallengeTypes = "body" | "eye" | "pray" | "hand" | "talk" | "meditation"