import { createContext, useState, useEffect } from 'react'
import { ChallengeProviderProps, ChallengeContextData, Challenge } from './interface'
import challenges from './challenges.json'
import Cookies from 'js-cookie'
import LevelUpModal from '../../components/LevelUpModal'

export const ChallengesContext = createContext({} as ChallengeContextData)

const ChallengesProvider = ({ 
  children, 
  ...rest
}: ChallengeProviderProps) => {
  const [level, setLevel] = useState(rest.level ?? 1)
  const levelExperience = (lvl: number): number => lvl > 1 ? Math.pow((lvl) * 4, 2) : 0

  const [currentExperience, setCurrentExperience] = useState(rest.currentExperience ?? 0)
  const [experienceCurrentLevel, setExperienceCurrentLevel] = useState(levelExperience(level))
  const [experienceNextLevel, setExperienceNextLevel] = useState(levelExperience(level + 1))
  const [challengesCompleted, setChallengesCompleted] = useState<number>(rest.challengesCompleted ?? 0)

  const [activeChallenge, setActiveChallenge] = useState<Challenge>(null)

  const [isLevelUpModalOpen, setIsLevelUpModalOpen] = useState<boolean>(false)

  useEffect(() => {
    Notification.requestPermission()
  }, [])

  useEffect(() => {
    Cookies.set('level', String(level))
    Cookies.set('currentExperience', String(currentExperience))
    Cookies.set('challengesCompleted', String(challengesCompleted))
  }, [level, currentExperience, challengesCompleted])

  const levelUp = () => {
    const nextLevel = level + 1
    setLevel(nextLevel)
    setExperienceCurrentLevel(experienceNextLevel)
    setExperienceNextLevel(levelExperience(nextLevel + 1))
    setIsLevelUpModalOpen(true)
  }

  const startNewChallenge = () => {
    const randomChallengeIndex = Math.floor(Math.random() * challenges.length)
    const newChallenge = challenges[randomChallengeIndex]
    setActiveChallenge(newChallenge as Challenge)

    new Audio('/assets/beep-01.wav').play()

    if(Notification.permission === "granted") {
      new Notification("Novo desafio! 💪", {
        body: `Desafio '${newChallenge.type}' valendo ${newChallenge.amount}XP.\n\n${newChallenge.description}`
      })
    }
  }

  useEffect(() => {
   if (currentExperience >= experienceNextLevel) {
      levelUp()
    }
  }, [currentExperience, experienceNextLevel])

  const challengeCompleted = (exp: number) => {
    const nextExp = currentExperience + exp
    setCurrentExperience(nextExp)
    setChallengesCompleted((prev) => prev + 1)
    resetChallenge()
  }

  const resetChallenge = () => setActiveChallenge(null)

  const closeLevelUpModal = () => setIsLevelUpModalOpen(false)

  return (
    <ChallengesContext.Provider 
      value={{ 
        level, 
        currentExperience,
        experienceCurrentLevel,
        experienceNextLevel,
        challengesCompleted,
        activeChallenge,
        levelUp,
        startNewChallenge,
        challengeCompleted,
        resetChallenge,
        closeLevelUpModal,
      }}>
      {children}
      {isLevelUpModalOpen && <LevelUpModal />}
    </ChallengesContext.Provider>
  )
}

export default ChallengesProvider